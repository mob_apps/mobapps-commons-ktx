package com.mobapps.commons.mobapps_ktx.helpers

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

object MobAppsAnalytics {

    private var analytics: FirebaseAnalytics? = null

    fun initialize(context: Context) {
        if (analytics == null) {
            analytics = FirebaseAnalytics.getInstance(context)
        }
    }

    @JvmOverloads
    fun sendEvent(event: String, bundle: Bundle? = null) {
        analytics?.logEvent(event, bundle)
    }

}