package com.mobapps.commons.mobapps_ktx.exts

import androidx.viewpager.widget.ViewPager

fun ViewPager.isLastItem() = currentItem == adapter?.count?.minus(1)

fun ViewPager.isFirstItem() = currentItem == 0

fun ViewPager.lastItem() = if (currentItem > 0) currentItem-1 else 0

@JvmOverloads fun ViewPager.previousItem(smoothScroll: Boolean = true) = setCurrentItem(lastItem(), smoothScroll)