package com.mobapps.commons.mobapps_ktx.exts

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle

inline fun <reified T : Any> Activity.initActivity(
    requestCode: Int = -1,
    options: Bundle? = null,
    noinline init: Intent.() -> Unit = {}) {
    val intent = newIntent<T>(this)
    intent.init()
    startActivityForResult(intent, requestCode, options)
}

inline fun <reified T : Any> Context.initActivity(
    options: Bundle? = null,
    noinline init: Intent.() -> Unit = {}) {
    val intent = newIntent<T>(this)
    intent.init()
    startActivity(intent, options)
}

inline fun <reified T : Any> newIntent(context: Context): Intent = Intent(context, T::class.java)

@JvmOverloads fun Activity.shareApp(
    title: String = "Enviar Para",
    message:String =
        "Baixe agora o Aplicativo ${appName()} https://play.google.com/store/apps/details?id=$packageName"
) {
    val pm: PackageManager = packageManager
    try {
        val whatsAppIntent = Intent(Intent.ACTION_SEND)
        whatsAppIntent.type = "text/plain"
        val info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)
        whatsAppIntent.setPackage(info.packageName)
        whatsAppIntent.putExtra(Intent.EXTRA_TEXT, message)
        startActivity(Intent.createChooser(whatsAppIntent, title))
    } catch (e: PackageManager.NameNotFoundException) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(
            Intent.EXTRA_TEXT,
            message
        )
        sendIntent.type = "text/plain"
        startActivity(Intent.createChooser(sendIntent, title))
    }
}


fun Context.appName() = applicationInfo.loadLabel(packageManager)