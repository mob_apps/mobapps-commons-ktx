package com.mobapps.commons.mobapps_ktx.helpers

import android.os.Bundle

fun sendEvent(event: String, bundle: Bundle.() -> Unit = {}) {
    MobAppsAnalytics.sendEvent(event, Bundle().also(bundle))
}