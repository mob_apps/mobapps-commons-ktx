package com.mobapps.commons.mobapps_ktx.exts

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.drawable.VectorDrawable
import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat

fun Context.launchUrl(url: String) {
    val uri = Uri.parse(url)
    try {
        startActivity(Intent(Intent.ACTION_VIEW, uri))
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

@JvmOverloads fun Context.launchGooglePlay(appPackageName: String = packageName) {
    var uri = Uri.parse("market://details?id=$appPackageName")
    val intent = Intent(Intent.ACTION_VIEW, uri)
    try {
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        uri = Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
        startActivity(Intent(Intent.ACTION_VIEW, uri))
    }
}

@JvmOverloads fun Context.openAppOrLaunchPlay(appPackageName: String = packageName) {
    val intent = packageManager.getLaunchIntentForPackage(appPackageName)
    if (intent != null) {
        startActivity(intent)
    } else {
        launchGooglePlay(appPackageName)
    }
}

fun Context.isDarkMode() : Boolean {
    val nightModeFlags: Int = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
    return nightModeFlags == Configuration.UI_MODE_NIGHT_YES
}

/**
 * Creates a [VectorDrawableCompat] from a drawable resource to use as
 * a regular drawable in views programmatically.
 *
 * Make sure you supply a proper [VectorDrawable] via [resId].
 * Any attempt to use this with a regular image (PNG, JPEG, GIF, WEBP)
 * resource will result in a crash.
 *
 * @param resId VectorDrawable XML resource
 * @returns a [VectorDrawableCompat] or null if parsing error is found
 */
fun Context.createVectorDrawable(@DrawableRes resId: Int ) : VectorDrawableCompat? {
    return VectorDrawableCompat.create(resources, resId, theme)
}

fun Fragment.createVectorDrawable(@DrawableRes resId: Int ) : VectorDrawableCompat? {
    return requireContext().createVectorDrawable(resId)
}