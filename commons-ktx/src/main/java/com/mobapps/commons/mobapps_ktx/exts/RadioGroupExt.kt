package com.mobapps.commons.mobapps_ktx.exts

import android.widget.RadioButton
import android.widget.RadioGroup

fun RadioGroup.wasSelected() = checkedRadioButtonId > 0

fun RadioGroup.getRadioButtonSelected(): RadioButton? {
    return findViewById(checkedRadioButtonId)
}