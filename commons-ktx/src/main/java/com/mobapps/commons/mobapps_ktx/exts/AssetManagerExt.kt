package com.mobapps.commons.mobapps_ktx.exts

import android.content.res.AssetManager
import java.nio.charset.Charset

fun AssetManager.loadJsonString(fileName: String): String? {
    try {
        val inputStream = open(fileName)
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()

        return String(buffer, Charset.forName("UTF-8"))
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return null

}